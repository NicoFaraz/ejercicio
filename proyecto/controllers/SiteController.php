<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FormPersona;
use app\models\Persona;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function actionAgregar() {
        $model = new FormPersona;
        $msg = null;
        $query = new Query();
        $query->select(['p.id, CONCAT(p.apellido,", ",p.nombre) AS amigo'])
                ->from('persona p');
        $command = $query->createCommand();
        $amigos = $command->queryAll();
        $lista_amigos = ArrayHelper::map($amigos, 'id', 'amigo');
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $table = new Persona;
                $table->nombre = $model->nombre;
                $table->apellido = $model->apellido;
                $table->edad = $model->edad;
                $table->dni = $model->dni;
                $table->email = $model->email;
                $table->amigo_id = $model->amigo_id;
                if ($table->insert()) {
                    Yii::$app->session->setFlash('personaAgregada');
                    $msg = "Enhorabuena registro guardado correctamente";
                    $model->nombre = null;
                    $model->apellido = null;
                    $model->edad = null;
                    $model->dni = null;
                    $model->email = null;
                    $model->amigo_id = null;
                    header('index.php?r=agregar');
                } else {
                    Yii::$app->session->setFlash('personaAgregadaError');
                    $msg = "Ha ocurrido un error al insertar el registro";
                    header('index.php?r=agregar');
                }
            } else {
                $model->getErrors();
            }
        }
        return $this->render("agregar", ['model' => $model, 'msg' => $msg, 'listaAmigos' => $lista_amigos]);
    }

    public function actionListar() {
        $query = new Query();
        $query->select(['p.*, CONCAT(a.apellido,", ",a.nombre) AS amigo'])
                ->from('persona p')
                ->leftJoin('persona a', 'a.id = p.amigo_id');
        $command = $query->createCommand();
        $rows = $command->queryAll();
        
        return $this->render("listar", ["contanctos" => $rows]);
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
