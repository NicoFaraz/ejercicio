<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Nicolas Faraz';
$this->params['breadcrumbs'][] = 'Agregar';
?>
<div class="site-contact">
    <h1><?= Html::encode("Agregar") ?></h1>

    <?php if (Yii::$app->session->hasFlash('personaAgregada')): ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg ?>
        </div>
        <a class="btn btn-xs btn-success" href="index.php?r=site/agregar">Agregar</a>
    <?php elseif (Yii::$app->session->hasFlash('personaAgregadaError')): ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg ?>
        </div>
        <a class="btn btn-xs btn-success" href="index.php?r=site/agregar">Agregar</a>
    <?php else: ?>
        <p>Complete los siguientes campos.</p>
        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'nombre')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'apellido')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'edad') ?>
                <?= $form->field($model, 'dni') ?>
                <?= $form->field($model, 'email') ?>
                <?=$form->field($model, 'amigo_id')->dropDownList($listaAmigos, ['prompt' => 'Seleccione Uno' ])->label('Amigo');?>
                <div class="form-group">
                    <?= Html::submitButton('Agregar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
