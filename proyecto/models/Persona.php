<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellido
 * @property int $dni
 * @property int $edad
 * @property string|null $email
 * @property int|null $amigo_id
 *
 * @property Persona $amigo
 * @property Persona[] $personas
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'dni', 'edad'], 'required'],
            [['dni', 'edad', 'amigo_id'], 'integer'],
            [['nombre', 'apellido'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 100],
            [['amigo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['amigo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'dni' => 'Dni',
            'edad' => 'Edad',
            'email' => 'Email',
            'amigo_id' => 'Amigo ID',
        ];
    }

    /**
     * Gets query for [[Amigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmigo()
    {
        return $this->hasOne(Persona::className(), ['id' => 'amigo_id']);
    }

    /**
     * Gets query for [[Personas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['amigo_id' => 'id']);
    }
}
