<?php

namespace app\models;

use Yii;
use yii\base\model;

class FormPersona extends model {

    public $nombre;
    public $apellido;
    public $edad;
    public $dni;
    public $email;
    public $amigo_id;

    public function rules() {
        return [
            ['nombre', 'required', 'message' => 'Campo requerido'],
            ['nombre', 'match', 'pattern' => '/^[a-záéíóúñ\s]+$/i', 'message' => 'Sólo se aceptan letras'],
            ['nombre', 'match', 'pattern' => '/^.{3,50}$/', 'message' => 'Mínimo 3 máximo 50 caracteres'],
            ['apellido', 'required', 'message' => 'Campo requerido'],
            ['apellido', 'match', 'pattern' => '/^[a-záéíóúñ\s]+$/i', 'message' => 'Sólo se aceptan letras'],
            ['apellido', 'match', 'pattern' => '/^.{3,80}$/', 'message' => 'Mínimo 3 máximo 80 caracteres'],
            ['edad', 'required', 'message' => 'Campo requerido'],
            ['edad', 'integer', 'message' => 'Sólo números enteros', 'min' => '18', 'message' => 'Debe ser mayor de 18 años'],
            ['dni', 'required', 'message' => 'Campo requerido'],
            ['dni', 'integer', 'message' => 'Sólo números enteros'],
            ['email', 'required', 'message' => 'Campo requerido'],
            ['email', 'match', 'pattern' => '/^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/', 'message' => 'El formato es incorrecto'],
            ['amigo_id', 'integer'],
        ];
    }

}
